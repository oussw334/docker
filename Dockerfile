FROM nginx:alpine
LABEL maintainer="o.aztouti@dictu.nl"
WORKDIR /tmp
COPY important/ .


# Install dependencies
RUN apk add --update --no-cache \
            ca-certificates \
            libpcap \
            curl util-linux \
            libcurl \
            libxml2-utils \
            python3-dev \
            nmap \
            nmap-scripts \
            nikto \
            bind-tools \
            py3-pip \
            bash \
            bash-completion \
            util-linux \
            pciutils \
            usbutils \
            coreutils \
            binutils \
            findutils \
            grep \
            procps \
            drill \
            git \
            tzdata \
            cmake \
            make \
            musl-dev \
            gcc \
            gettext-dev \
            libintl \
            coreutils \
            libidn \
            openssl \
            libstdc++ \
            chromium \
            chromium-chromedriver \
            harfbuzz \
            nss \
            freetype \
            ttf-freefont \
            perl\
            perl-net-ssleay \
            python3-dev \
            htop \
            bind-tools \
            ruby-dev \
            ruby-bundler \
            libxslt-dev \
            libxml2-dev \
            g++ \
            texlive-xetex \
            texmf-dist-latexextra \
            && update-ca-certificates \
            && rm -rf /var/cache/apk/* \
            && apk add --no-cache --virtual .build-dependencies build-base curl-dev \
            && pip3 install --upgrade pip \
            && pip3 install pycurl \
            && apk del .build-dependencies \
            && pip3 install wfuzz

RUN pip3 install -r import-extra.txt
RUN pip3 install urllib3
RUN pip3  install requests
RUN cp /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime
RUN echo "Europe/Amsterdam" >  /etc/timezone
ENV MUSL_LOCPATH /usr/share/i18n/locales/musl
ENV LC_ALL nl_NL.UTF-8
ENV LANG=nl_NL.UTF-8 \
    LANGUAGE=nl_NL.UTF-8

#beperkte set aan locales ophalen en er in batsen
#RUN unzip musl-locales-master.zip \
#      && cd musl-locales-master \
#      && cmake -DLOCALE_PROFILE=OFF -D CMAKE_INSTALL_PREFIX:PATH=/usr . && make && make install \
#      && cd .. && rm -r musl-locales-master.zip

#chromium
#ENV CHROME_BIN=/usr/bin/chromium-browser \
#    CHROME_PATH=/usr/lib/chromium/
#ENV DISPLAY=:99
#ENV PATH /usr/local/texlive/2020/bin/x86_64-linuxmusl/:$PATH
#COPY texfm/ /root/texfm


RUN mkdir /home/secter
WORKDIR /home/secter/

RUN git clone --depth=1 https://github.com/maurosoria/dirsearch.git dirsearch \
	&& git clone --depth=1 https://github.com/sqlmapproject/sqlmap.git sqlmap-dev \
      && git clone --depth=1 https://github.com/offensive-security/exploitdb.git 


#git meukapk addap
RUN git clone --branch=master \
               --depth=1 \
               https://github.com/urbanadventurer/WhatWeb.git \
	&& git clone https://github.com/drwetter/testssl.sh.git \
	&& git clone --branch=master \
               --depth=1 \
			   https://github.com/arthepsy/ssh-audit.git \
	&& git clone https://github.com/darkoperator/dnsrecon 

RUN cd /home/secter/WhatWeb && bundle install

#symlink de shit
RUN ln -s /home/secter/exploitdb/searchsploit /usr/local/bin/searchsploit
RUN ln -s /home/secter/sslscan/sslscan /usr/local/bin/sslscan
RUN ln -s /home/secter/WhatWeb/whatweb /usr/local/bin/whatweb
RUN ln -s /home/secter/testssl.sh/testssl.sh /usr/local/bin/testssl.sh
RUN ln -s /home/secter/sqlmap-dev/sqlmap.py /usr/local/bin/sqlmap
#dirty python fix voor ssh-audit
RUN cp /usr/bin/python3 /usr/local/bin/python
RUN ln -s /home/secter/ssh-audit/ssh-audit.py /usr/local/bin/ssh-audit
RUN ln -s /home/secter/dnsrecon/dnsrecon.py /usr/local/bin/dnsrecon

# van de enige echte JK
